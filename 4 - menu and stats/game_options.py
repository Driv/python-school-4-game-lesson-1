import pygame
import os
import math

pygame.init()
clock = pygame.time.Clock()

black = (0, 0, 0)
red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)
screen_size = (800, 600)
tile_size = 32
board_size = (20, 15)  # в тайлах
right_menu_size = (screen_size[0] - board_size[0] * tile_size, screen_size[1])
bottom_menu_size = (screen_size[0], screen_size[1] - board_size[1] * tile_size)

mouse_coord = (screen_size[0] // 2, screen_size[1] // 2)
player_coord = (screen_size[0] // 2, screen_size[1] // 2)
player_speed = 5
bullet_speed = 10

tileset = pygame.image.load(os.path.dirname(os.getcwd()) + '\\resources\\tileset.png')
bulletset = pygame.image.load(os.path.dirname(os.getcwd()) + '\\resources\\bullet.png')


def get_tile(x, y, some_tileset):
    return some_tileset.subsurface(x * tile_size, y * tile_size, tile_size,
                                   tile_size)


tiles = {
    # 'grass': [get_tile(x, 13, tileset) for x in range(0, 7)].extend([get_tile(x, 15, tileset) for x in range(0, 7)])
    'grass_bad': [get_tile(x, 13, tileset) for x in range(0, 7)],
    'grass_good': [get_tile(x, 15, tileset) for x in range(0, 7)],
    # 'arrow': [get_tile(10, 40, tileset)]
    'arrow': [get_tile(63, 9, tileset)] + [get_tile(x, 10, tileset) for x in range(8, 15)],
    'rock': [get_tile(x, 11, tileset) for x in range(8, 10)],
    'hero': [get_tile(4, 2, tileset)]

}

class Projectile(pygame.sprite.Sprite):
    def __init__(self, coord1, coord2, speed, damage, image, surface):
        super.__init__(self)
        self.image = image
        self.surface = surface
        self.damage = damage
        self.start = coord1
        self.end = coord2
        self.coord = self.start
        self.speed = speed
        if self.end[0] - self.start[0] == 0:
            self.angle = math.pi / 2
        else:
            self.angle = math.atan((self.end[1] - self.start[1]) / (self.end[0] - self.start[0]))
        if self.end[0] - self.start[0] < 0:
            self.angle += math.pi

    def move(self):
        self.coord = (
        self.coord[0] + self.speed * math.cos(self.angle), self.coord[1] + self.speed * math.sin(self.angle))




screen = pygame.display.set_mode(screen_size)
board = pygame.Surface((board_size[0] * tile_size, board_size[1] * tile_size))
right_menu = pygame.Surface(right_menu_size)
bottom_menu = pygame.Surface(bottom_menu_size)
