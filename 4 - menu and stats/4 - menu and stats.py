import pygame
import random
from game_options import *


def redraw_board():
    # draw grass
    for x in range(0, board_size[0] + 1):
        for y in range(0, board_size[1] + 1):
            '''board.blit(random.choice(tiles['grass_bad']),
                       (x * tile_size, y * tile_size, tile_size, tile_size))'''
            board.blit(tiles['grass_good'][0],(x * tile_size, y * tile_size, tile_size, tile_size))
    for b in bullets:
        board.blit(tiles['rock'][0], b.coord)

    # draw hero on custom coord
    board.blit(tiles['hero'][0], player_coord)


def redraw_menu():
    right_menu.fill(green)
    bottom_menu.fill(black)
    bottom_menu.blit(tiles['arrow'][0], (0 * tile_size, 0))
    bottom_menu.blit(tiles['rock'][0], (1 * tile_size, 0))
    bottom_menu.blit(tiles['rock'][1], (2 * tile_size, 0))


bullets = []
game_loop = True
while game_loop:
    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            game_loop = False
    mouse_coord = pygame.mouse.get_pos()
    mouse_pressed = pygame.mouse.get_pressed()
    pressed = pygame.key.get_pressed()
    dx = dy = 0
    if pressed[pygame.K_w]: dy = -player_speed
    if pressed[pygame.K_s]: dy = +player_speed
    if pressed[pygame.K_a]: dx = -player_speed
    if pressed[pygame.K_d]: dx = +player_speed

    if mouse_pressed[0]:
        bullets.append(Projectile(player_coord,mouse_coord,bullet_speed))


    player_coord = (player_coord[0] + dx, player_coord[1] + dy)
    for x in bullets: x.move()
    redraw_board()
    screen.blit(board, (0, 0))
    redraw_menu()

    screen.blit(right_menu, (board.get_rect().width, 0))
    screen.blit(bottom_menu, (0, board.get_rect().height))
    pygame.display.flip()

    clock.tick(60)

pygame.quit()
