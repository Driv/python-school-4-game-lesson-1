'''
[классы]
классы - логически сгруппированный набор функций и переменных
объявление класса - своего рода "чертежи" будущих объектов, которые надо создавать
'''


class CarBlueprint:
    def set_wheel(self, wheel):
        self.wheel = wheel

    def set_name(self, name):
        self.name = name

    def get_info(self):
        print('Кол-во колес у авто {}: {}'.format(self.name, self.wheel))

    def drive(self):
        if self.wheel and self.name:
            print(self.name + ' завелась и поехала')
        else:
            print('У нашего авто нет колес или наименования')


# создаем экземпляры классов по "чертежам"
car1 = CarBlueprint()
car1.set_wheel(4)
car1.set_name('Mersedes')
car2 = CarBlueprint()
car2.set_wheel(4)
car2.set_name('Audi')
car1.get_info()
car2.get_info()

'''
self - параметр, который ссылается на данный на данный экземпляр класса
когда мы вызываем функцию экземпляра класса, ее манипуляции должны быть над данным экземпляром
вызов car1.set_name('Mersedes') это сокращение вызова CarBlueprint.set_name(car1,'Mersedes')

__init__ - функция, которая вызывается при создании экземпляра класса
в текущем виде нашего класса нам надо обязательно указывать кол-во колес и наименование уже после создания экземпляра класса
если мы захотим вывести информацию об авто (get_info), не указав до этого кол-во колес и наименование, то информация будет неполной
чтобы этого избежать, можно переопределить функцию __init__ с указанием этих данных при создании
пример:

def __init__(self, new_name, new_wheel):
    self.name = new_name
    self.wheel = new_wheel
    
создадим новый класс с использованием этих доработок
'''


class CarNewBlueprint:
    def __init__(self, new_name, new_wheel):
        self.wheel = new_wheel
        self.name = new_name

    def get_info(self):
        print('Кол-во колес у авто {}: {}'.format(self.name, self.wheel))

    def drive(self):
        if self.wheel and self.name:
            print(self.name + ' завелась и поехала')
        else:
            print('У нашего авто нет колес или наименования')


# обратите внимание на то,как теперь создаются экземпляры нового класса CarNewBlueprint

car3 = CarNewBlueprint('Mitsubishi', 4)
car4 = CarNewBlueprint('Honda', 2)
car3.get_info()
car4.get_info()

'''
наследование классов
предположим, что у нас есть базовый чертеж автомобиля, в котором мы указываем кол-во колес и марку,
при этом он может выводить информацию о себе и ехать
нам надо сделать чертежи грузовиков, которые могут перевозить грузы, и полицейских машин, которые могут включать мигалку
при этом они должны также иметь колеса, выводить информацию о себе и ехать
но полицейская машина не должна перевозить грузы, а у грузовиков нет мигалок

наследование от класса перенимает в новый чертеж все старые свойства класса-предка
опишем два новых класса, унаследованных от базового класса 
'''


class TruckBlueprint(CarNewBlueprint):
    items = []

    def get_items(self):
        for i in self.items:
            print(i)

    def add_item(self, item):
        self.items.append(item)

    def unload_items(self):
        self.items = []


class PoliceBlueprint(CarNewBlueprint):
    police_light = False

    def light_on(self):
        self.police_light = True

    def light_off(self):
        self.police_light = False


'''
теперь создадим экземпляры новых классов. 
обратите внимание, что создаются они с тем же списком параметров, что и их предок - класс CarNewBlueprint
'''
police = PoliceBlueprint('special_police_car', 6)
truck = TruckBlueprint('volvo', 12)
# теперь попробуем вывести информацию о них и поехать (выполнить функции их предка - класса CarNewBlueprint)
police.get_info()
police.drive()
truck.get_info()
truck.drive()

