class Velik:
    wheel = 2
    seat = 1
    krik = 'aaa'
    name = 'test'
    drive = False

    def engine(self):
        self.drive = True

    def scream(self):
        print(self.krik)

    def set_krik(self, noviy_krik):
        self.krik = noviy_krik

    def set_seat(self, new_seat):
        self.seat = new_seat

    def info(self):
        print('в этом велосипеде колес: ' + str(self.wheel))
        print('его имя: ' + self.name)
        print('сидений: ' + str(self.seat))

    def new_name(self, name):
        self.name = name


class WarVelik(Velik):
    guns = 1
    armor = 1
    mines = 1
    attack_speed = 30
    vistrel = 'bam'

    def set_guns(self, new_guns):
        self.guns = new_guns

    def set_armor(self, new_armor):
        self.armor = new_armor

    def set_mines(self, new_mines):
        self.mines = new_mines

    def set_attack_speed(self, new_attack_speed):
        self.attack_speed = new_attack_speed

    def set_vistrel(self, new_vistrel):
        self.vistrel = new_vistrel


velik1 = Velik()
velik1.scream()
velik2 = Velik()
velik2.set_krik('RRRAAAAAAA')
velik2.info()
velik1.info()
